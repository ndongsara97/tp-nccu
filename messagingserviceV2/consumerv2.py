import paho.mqtt.client as mqtt
import logging
from google.cloud import pubsub_v1

# Google Cloud Pub/Sub Configuration
project_id = "tpnccu"
topic_id = "pressure_temperature"
publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

# Mosquitto Broker Configuration
mqtt_broker = "mosquitto"  # Mosquitto container IP
mqtt_port = 1883

# Configure logging
logging.basicConfig(level=logging.DEBUG)

def on_message(client, userdata, msg):
    message = msg.payload.decode("utf-8")
    logging.info(f"Received message from Mosquitto: {message}")

    try:
        logging.info("Publishing to Google Cloud Pub/Sub...")
        # Data must be a bytestring
        data = message.encode("utf-8")
        # When you publish a message, the client returns a future.
        future = publisher.publish(topic_path, data)
        logging.info(f"Published message to Google Cloud Pub/Sub: {future.result()}")

    except Exception as e:
        logging.error(f"Error publishing to Google Cloud Pub/Sub: {e}")

# Connect to Mosquitto
try:
    logging.info("Connecting to Mosquitto...")
    client = mqtt.Client()
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    logging.info("Connected to Mosquitto!")

    # Subscribe to the "pressure_temperature" topic
    logging.info("Subscribing to 'pressure_temperature' topic...")
    client.subscribe("pressure_temperature", 1)
    logging.info("Subscribed to 'pressure_temperature' topic!")

    # Start the MQTT loop
    logging.info("Starting MQTT loop...")
    client.loop_forever()

except Exception as e:
    logging.error(f"Error connecting to Mosquitto: {e}")

