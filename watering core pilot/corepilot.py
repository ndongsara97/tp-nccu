# -*- coding: utf-8 -*-

import ssl
import json
import logging
import paho.mqtt.client as mqtt
import os

# Set the SSL_PASSPHRASE environment variable
os.environ['SSL_PASSPHRASE'] = '1234'

# Configuration du broker MQTT
mqtt_broker = "192.168.2.1" # NGINX ADDRESS 
mqtt_topic = "pressure_temperature"  

# Configuration des logs
logging.basicConfig(level=logging.INFO)

# Create an SSL context
context = ssl.create_default_context()
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE

# Set the location of the CA certificate
context.load_verify_locations(cafile="./ca/ca.crt")

# Set the location of the client certificate and key
context.load_cert_chain(certfile="./ca/publisher.crt", keyfile="./ca/publisher.key", password=os.environ.get('SSL_PASSPHRASE'))


def on_connect(client, userdata, flags, rc):
    logging.info("Connecté au broker avec le code de retour {rc}")
    client.subscribe(mqtt_topic)

def on_message(client, userdata, msg):
    try:
        # Décoder le message JSON
        message_data = json.loads(msg.payload.decode("utf-8"))

        # Récupérer la propriété 'system_status'
        system_status = message_data.get("system_status", "N/A")

        # Afficher le statut du système dans les logs
        if system_status.lower() == "enabled":
            logging.info("Le système est activé (enable).")
        elif system_status.lower() == "disabled":
            logging.info("Le système est désactivé (disable).")
        else:
            logging.warning("Statut du système inconnu : {system_status}")

    except json.JSONDecodeError as e:
        logging.error("Erreur de décodage JSON : {e}")
    except Exception as e:
        logging.error("Erreur inattendue : {e}")

# Créer un client MQTT
client = mqtt.Client()
client.tls_set_context(context)

# Configurer les callbacks
client.on_connect = on_connect
client.on_message = on_message

# Se connecter au broker
client.connect(mqtt_broker, 8883, 60)

# Boucle de communication MQTT
client.loop_forever()

