import pika
from pymongo import MongoClient
from pika import PlainCredentials

# RabbitMQ parameters
rabbitmq_broker = "192.168.1.2"  # RabbitMQ container IP
rabbitmq_port = 5672
rabbitmq_queue = "computation_result"
#rabbitmq_routing_key = "ptcr"
rabbitmq_username = "clope"
rabbitmq_password = "clope"

# MongoDB parameters
mongo_user = "root"
mongo_password = "example"
database_name = "PTCR"
collection_name = "PTCRs"

# Establish a connection to RabbitMQ
credentials = PlainCredentials(rabbitmq_username, rabbitmq_password)
parameters = pika.ConnectionParameters(rabbitmq_broker, rabbitmq_port, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

# Declare your queue
channel.queue_declare(queue=rabbitmq_queue, durable=True)

# Establish a connection to MongoDB
client = MongoClient('mongodb://root:example@192.168.1.3:27017/', authSource='admin')
db = client[database_name]
collection = db[collection_name]

def callback(ch, method, properties, body):
    print(f"Received {body}")
    # Insert the message into the MongoDB collection
    collection.insert_one({"message coming": body.decode()})
    print("Message saved to MongoDB")

channel.basic_consume(queue=rabbitmq_queue, on_message_callback=callback, auto_ack=True)

print('Waiting for messages. To exit press CTRL+C')
channel.start_consuming()

