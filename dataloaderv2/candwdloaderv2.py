import os
import time
import json
import logging
import redis
from google.cloud import datastore
from google.oauth2 import service_account
from google.cloud.sql.connector import Connector, IPTypes
import pg8000
import sqlalchemy

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Google Cloud Datastore Parameters
project_id = 'tpnccu'  # Replace with your project ID

# Redis Connection Parameters
redis_host = '192.168.1.17'
redis_port = '6379'
redis_password = ''

# Database connection parameters for Cloud SQL
db_name = 'weatherconfig'
db_user = 'postgres'
db_pass = 'clope'
instance_connection_name = 'tpnccu:us-central1:tpnccu'

# Function to get database connection pool
def get_db_connection():
    # Determine IP type (PRIVATE or PUBLIC) based on environment variable
    ip_type = IPTypes.PRIVATE if os.environ.get("PRIVATE_IP") else IPTypes.PUBLIC
    connector = Connector()

    def getconn() -> pg8000.dbapi.Connection:
        conn: pg8000.dbapi.Connection = connector.connect(
            instance_connection_name,
            "pg8000",
            user=db_user,
            password=db_pass,
            db=db_name,
            ip_type=ip_type,
        )
        return conn

    pool = sqlalchemy.create_engine(
        "postgresql+pg8000://",
        creator=getconn,
    )
    return pool

get_stmt = insert_stmt = sqlalchemy.text(
    "SELECT * FROM config",
)


def get_postgres_data():
    db = get_db_connection()
    with db.connect() as conn:
        result = conn.execute(get_stmt)
        return result.fetchall()

def get_datastore_data():
    credentials = service_account.Credentials.from_service_account_file('/app/datastoreserviceaccount.json')
    datastore_client = datastore.Client(project=project_id, credentials=credentials)

    query = datastore_client.query(kind='PTCRs')
    results = query.fetch()

    data = []
    for entity in results:
        data.append(dict(entity))
    return data

def convert_postgres_text_to_dict(postgres_data):
    if postgres_data:
        json_data_text = postgres_data[0][0]
        return json.loads(json_data_text)
    return {}

def cache_data_in_redis(mongodb_data, postgres_data):
    r = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

    for i, document in enumerate(mongodb_data):
        key = f'resultandcommandsid{i+1}'
        r.set(key, json.dumps(document))

    config_data_dict = convert_postgres_text_to_dict(postgres_data)
    r.set('config', json.dumps(config_data_dict))

def main():
    r = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

    while True:
        try:
            datastore_data = get_datastore_data()
            postgres_data = get_postgres_data()
            logging.info("Fetched Datastore data and PostgreSQL config data")
            for document in datastore_data:
                logging.info(document)
            logging.info(postgres_data)

            cache_data_in_redis(datastore_data, postgres_data)

        except Exception as e:
            logging.error(f"Error occurred: {e}")

        time.sleep(10)

if __name__ == "__main__":
    main()

