import paho.mqtt.client as mqtt
import logging
import ssl
import os
import json
import time
import random

logging.basicConfig(level=logging.DEBUG)

# MQTT Broker Settings
broker_address = "192.168.3.1"
broker_port = 8883  # NGINX SSL/TLS port

# Create an SSL context
context = ssl.create_default_context()
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE

# Set the location of the CA certificate
context.load_verify_locations(cafile="./ca/ca.crt")

# Set the location of the client certificate and key
context.load_cert_chain(certfile="./ca/publisher.crt", keyfile="./ca/publisher.key", password=os.environ.get('SSL_PASSPHRASE'))

# Create an MQTT client and connect to the broker
client = mqtt.Client()
client.tls_set_context(context)

# Add these lines before the connect statement
client.on_connect = lambda client, userdata, flags, rc: print(f"Connected with result code {rc}")
client.on_log = lambda client, userdata, level, buf: print(f"Log: {buf}")

# Connect to the broker
client.connect(broker_address, broker_port, 60)

# Function to generate random data with an incrementing id
def generate_sensor_data(id_value):
    return {"id": id_value, "temperature": random.uniform(1, 20), "pressure": random.uniform(1, 10)}

# Run a loop to publish data every minute
id_counter = 1  # Initialize the id counter
while True:
    # Generate combined temperature and pressure data
    sensor_data = generate_sensor_data(id_counter)
    
    # Convert to JSON
    sensor_json = json.dumps(sensor_data)
    logging.info(sensor_json)
    # Publish to the "pressure_temperature" topic
    client.publish("pressure_temperature", payload=sensor_json, qos=1)

    # Increment the id counter
    id_counter += 1

    # Wait for a minute before publishing the next set of data
    time.sleep(10)

# Disconnect from the broker
client.disconnect()
