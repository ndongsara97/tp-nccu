from flask import Flask, request, jsonify
import requests

app = Flask(__name__)

# Define the mapping from /launcher/ paths to /api/control/ paths
PATH_MAPPING = {
    'automatic': '/api/control/0/',
    'manual/enable': '/api/control/1/',
    'manual/disable': '/api/control/2/'
}

def forward_request(target_path):
    # Build the full URL of the target API based on the provided path
    target_url = f"http://192.168.1.13:5000{target_path}"
    
    # Forward the request to the target API
    response = requests.post(target_url)
    
    # Return the response from the target API
    return response.text

@app.route('/launcher/automatic', methods=['POST'])
def launcher_automatic():
    return forward_request(PATH_MAPPING['automatic'])

@app.route('/launcher/manual/enable', methods=['POST'])
def launcher_manual_enable():
    return forward_request(PATH_MAPPING['manual/enable'])

@app.route('/launcher/manual/disable', methods=['POST'])
def launcher_manual_disable():
    return forward_request(PATH_MAPPING['manual/disable'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port =5000, debug=True)
