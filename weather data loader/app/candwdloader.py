from pymongo import MongoClient
import psycopg2
import redis
import json
import time

postgresdbname = 'bdsql'
postgres_user = 'na'
postgres_password = 'nam'
postgres_host = '192.168.1.16'
postgres_port = '5432'

redis_host = '192.168.1.17'
redis_port = '6379'
redis_password = ''

def get_mongodb_data():
    client = MongoClient('mongodb://root:example@192.168.1.3:27017/')
    db = client['PTRC']
    collection = db['PTRCs']
    data = collection.find()
    return data

def get_postgres_data():
    conn = psycopg2.connect(
        dbname=postgresdbname,
        user=postgres_user,
        password=postgres_password,
        host=postgres_host,
        port=postgres_port
    )
    cur = conn.cursor()
    cur.execute('SELECT * FROM config')
    data = cur.fetchall()
    return data

def convert_postgres_text_to_dict(postgres_data):
    if postgres_data:
        # Assuming the first column is the json_data column
        json_data_text = postgres_data[0][0]
        return json.loads(json_data_text)
    return {}

def cache_data_in_redis(mongodb_data, postgres_data):
    r = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

    for i, document in enumerate(mongodb_data):
        key = f'resultandcommandsid_{i}'
        r.set(key, json.dumps(document))

    config_data_dict = convert_postgres_text_to_dict(postgres_data)
    r.set('config', json.dumps(config_data_dict))

def main():
    while True:
        mongodb_data = get_mongodb_data()
        postgres_data = get_postgres_data()

        print("Hello, here is the MongoDB data and config data:")
        for document in mongodb_data:
            print(document)
        print(postgres_data)

        cache_data_in_redis(mongodb_data, postgres_data)

        # Add a delay of 60 seconds (adjust as needed)
        time.sleep(30)

if __name__ == "__main__":
    main()

