#!/bin/bash

# Définir les variables
DB_CONTAINER_NAME="postgres"
DB_USER="na"
DB_PASSWORD="nam"
DB_NAME="bdsql"
DB_PORT="5432"

# Créer le conteneur PostgreSQL
docker run -d \
  --name $DB_CONTAINER_NAME \
  -e POSTGRES_USER=$DB_USER \
  -e POSTGRES_PASSWORD=$DB_PASSWORD \
  -e POSTGRES_DB=$DB_NAME \
  -p $DB_PORT:5432 \
  postgres:latest

# Attendre quelques secondes pour que le conteneur démarre complètement
sleep 5

# Afficher les informations de connexion
echo "Conteneur PostgreSQL créé avec succès."
echo "Host: localhost"
echo "Port: $DB_PORT"
echo "Utilisateur: $DB_USER"
echo "Mot de passe: $DB_PASSWORD"
echo "Nom de la base de données: $DB_NAME"
