import pika
import paho.mqtt.client as mqtt

rabbitmq_broker = "192.168.1.2"  # RabbitMQ container IP
rabbitmq_port = 5672
rabbitmq_exchange = "weather"
rabbitmq_pressure_queue = "pressure"
rabbitmq_temperature_queue = "temperature"
rabbitmq_pressure_routing_key = "p"
rabbitmq_temperature_routing_key = "t"
rabbitmq_username = "clope"
rabbitmq_password = "clope"

mqtt_broker = "192.168.1.2"  # Mosquitto container IP
mqtt_port = 1883

def on_message(client, userdata, msg):
    message = msg.payload.decode("utf-8")
    print(f"Received message from Mosquitto: {message}")
    try:
        print("Connecting to RabbitMQ...")
        credentials = pika.PlainCredentials(username=rabbitmq_username, password=rabbitmq_password)
        connection_params = pika.ConnectionParameters(host=rabbitmq_broker, port=rabbitmq_port, credentials=credentials)
        connection = pika.BlockingConnection(connection_params)
        print("Connected to RabbitMQ!")

        channel = connection.channel()

        # Check if the message is from the "pressure" or "temperature" topic
        if msg.topic == "pressure":
            # For the "pressure" queue
            channel.queue_declare(queue=rabbitmq_pressure_queue, durable=True)
            channel.basic_publish(exchange=rabbitmq_exchange, routing_key=rabbitmq_pressure_routing_key, body=message)
        elif msg.topic == "temperature":
            # For the "temperature" queue
            channel.queue_declare(queue=rabbitmq_temperature_queue, durable=True)
            channel.basic_publish(exchange=rabbitmq_exchange, routing_key=rabbitmq_temperature_routing_key, body=message)

        # Close the RabbitMQ connection
        connection.close()
        print("Connection to RabbitMQ closed.")

    except Exception as e:
        print(f"Error connecting to RabbitMQ: {e}")

# Connect to Mosquitto
try:
    print("Connecting to Mosquitto...")
    client = mqtt.Client()
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    print("Connected to Mosquitto!")

    # Subscribe to the "pressure" topic
    print("Subscribing to 'pressure' topic...")
    client.subscribe("pressure", 1)
    print("Subscribed to 'pressure' topic!")

    # Subscribe to the "temperature" topic
    print("Subscribing to 'temperature' topic...")
    client.subscribe("temperature", 1)
    print("Subscribed to 'temperature' topic!")

    # Start the MQTT loop
    print("Starting MQTT loop...")
    client.loop_forever()

except Exception as e:
    print(f"Error connecting to Mosquitto: {e}")

