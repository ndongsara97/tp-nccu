package com.uservice.computeanalysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputeanalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComputeanalysisApplication.class, args);

    }



}
