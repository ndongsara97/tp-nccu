package com.uservice.computeanalysis.service;


import org.springframework.stereotype.Service;

@Service
public class ComputationService {


    public double performComputation(double pressure, double temperature) {
        // Perform the calculation using the provided parameters
        return (pressure*temperature) / 100;
    }
}
