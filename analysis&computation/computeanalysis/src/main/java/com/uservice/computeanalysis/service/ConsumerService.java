package com.uservice.computeanalysis.service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uservice.computeanalysis.model.MessageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ConsumerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerService.class);
    private final ObjectMapper objectMapper;
    private final ComputationService computationService;
    private final StringRedisTemplate redisTemplate;

    public ConsumerService(ObjectMapper objectMapper, ComputationService computationService, StringRedisTemplate redisTemplate) {
        this.objectMapper = objectMapper;
        this.computationService = computationService;
        this.redisTemplate = redisTemplate;
    }

    @RabbitListener(queues = "pressure_temperature")
    public void processWeatherMessage(Message message) {
        try {
            String messageBody = new String(message.getBody());
            MessageResult messageResult = objectMapper.readValue(messageBody, MessageResult.class);

            // Use ComputationService directly inside the listener method
            double result = computationService.performComputation(
                    messageResult.getPressure(),
                    messageResult.getTemperature()
            );

            messageResult.setResult(result);
            LOGGER.info("Received and processed message: {}", messageResult);

            // Save the MessageResult as a JSON string in Redis
            saveToRedis(messageResult);
        } catch (IOException e) {
            LOGGER.error("Error processing message: {}", e.getMessage());
        }
    }

    private void saveToRedis(MessageResult messageResult) {
        try {
            String key = "computationresult" + messageResult.getId();
            String value = objectMapper.writeValueAsString(messageResult);
            redisTemplate.opsForValue().set(key, value);
            LOGGER.info("Saved to Redis with key: {}", key);
        } catch (IOException e) {
            LOGGER.error("Error saving to Redis: {}", e.getMessage());
        }
    }
}
