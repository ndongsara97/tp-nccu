package com.uservice.computeanalysis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageResult {

    private String id;
    private double pressure;
    private double temperature;
    private double result;

}
