import json
import time
from google.cloud import pubsub_v1, firestore
import logging
import redis

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

# Google Cloud Pub/Sub settings
project_id = "tpnccu"  # replace with your GCP project ID
subscription_id = "uscomputation"
topic_name = "pressure_temperature"

# Redis settings
redis_host = "192.168.1.17"  # replace with your Redis server IP
redis_port = 6379

# Firestore settings
collection_name = "PTCRs"

# Create a subscriber client
subscriber = pubsub_v1.SubscriberClient()

# Create Firestore client
db = firestore.Client()

# Subscription path
subscription_path = subscriber.subscription_path(project_id, subscription_id)

# Create Redis client
redis_client = redis.Redis(host=redis_host, port=redis_port)

def perform_calculation(data_dict):
    temperature = data_dict.get("temperature", 0)
    pressure = data_dict.get("pressure", 0)
    result = temperature * pressure / 100
    return result

def callback(message):
    logging.info(f"Received message ID: {message.message_id}")
    data = message.data.decode("utf-8")
    logging.debug(f"Message data: {data}")

    # Parse message data from JSON
    try:
        data_dict = json.loads(data)
        logging.debug("Parsed message data from JSON.")
        # Perform calculation
        calculation_result = perform_calculation(data_dict)
        data_dict["result"] = calculation_result
        logging.info(f"Calculated result: {calculation_result}")

        # Save data to Redis
        redis_key = f"computationresult{data_dict['id']}"
        redis_client.set(redis_key, json.dumps(data_dict))
        logging.info(f"Saved to Redis under key: {redis_key}")
    except json.JSONDecodeError as e:
        logging.error(f"Error parsing message data from JSON: {e}")

    # Acknowledge the message
    message.ack()

# Subscribe to the topic
try:
    subscriber.subscribe(subscription_path, callback=callback)
    logging.info(f"Listening for messages on {subscription_path}...")
except Exception as e:
    logging.error(f"Error subscribing to topic: {e}")

# Keep the main thread alive, or the process will exit
try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    logging.info("Stopping the subscriber.")

