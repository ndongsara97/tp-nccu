from flask import Flask, request, g
import time
import redis
import json
import pika
import logging
import paho.mqtt.client as mqtt
from threading import Thread

app = Flask(__name__)

# Create a connection to the Redis server
r = redis.Redis(host='192.168.1.17', port=6379, db=0)
# Create a connection to RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.1.2'))
channel = connection.channel()
channel.queue_declare(queue="computation_result",durable=True)

# Create a connection to the MQTT broker
client = mqtt.Client()
client.connect("192.168.1.11", 1883, 60)
# logging 
logging.basicConfig(level=logging.INFO)
# Initialize the id
system_mode = 0
id = 1

def background_task():
    global system_mode, id
    while True:
        computation_result = r.get(f'computationresult{id}')
        logging.info(computation_result)
        config = r.get('config')
        logging.info(config)
        config_json = json.loads(config)
        if computation_result is not None:
            computation_result_json = json.loads(computation_result)
            if system_mode == 0:
                result = computation_result_json.get('result', 0)
                seuil = config_json.get('value',0)
                if result >= seuil:
                    computation_result_json['system_status'] = 'enabled'
                else:
                    computation_result_json['system_status'] = 'disabled'
            elif system_mode == 1:
                computation_result_json['system_status'] = 'enabled'
            elif system_mode == 2:
                computation_result_json['system_status'] = 'disabled'

            computation_result_str = json.dumps(computation_result_json)
            logging.info(computation_result_str)
            channel.basic_publish(exchange='PCTR', routing_key= 'pctr', body=computation_result_str)
            client.publish("computation_result", computation_result_str)
            id += 1
            time.sleep(10)
        else:
            time.sleep(10)


@app.route('/api/control/<int:mode>/', methods=['POST'])
def run_system_mode(mode):
    global system_mode, id
    system_mode = mode

    if system_mode == 0:
        return "Watering system status set to automatic"
    elif system_mode == 1:
        return "Watering system status set to enabled"
    elif system_mode == 2:
        return "Watering system status set to disabled"
    else:
        return "Invalid system mode"

@app.after_request
def after_request(response):
    if not hasattr(g, 'background_thread'):
        g.background_thread = Thread(target=background_task)
        g.background_thread.start()
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

