import pika
import paho.mqtt.client as mqtt
import logging

rabbitmq_broker = "rabbitmq"  # RabbitMQ container IP
rabbitmq_port = 5672
rabbitmq_exchange = "weather"
rabbitmq_pressure_temperature_queue = "pressure_temperature"
rabbitmq_pressure_temperature_routing_key = "#"
rabbitmq_username = "clope"
rabbitmq_password = "clope"

mqtt_broker = "mosquitto"  # Mosquitto container IP
mqtt_port = 1883

logging.basicConfig(level=logging.DEBUG)

def on_message(client, userdata, msg):
    message = msg.payload.decode("utf-8")
    logging.info(f"Received message from Mosquitto: {message}")
    try:
        print("Connecting to RabbitMQ...")
        credentials = pika.PlainCredentials(username=rabbitmq_username, password=rabbitmq_password)
        connection_params = pika.ConnectionParameters(host=rabbitmq_broker, port=rabbitmq_port, credentials=credentials)
        connection = pika.BlockingConnection(connection_params)
        print("Connected to RabbitMQ!")

        channel = connection.channel()

        # For the "pressure_temperature" queue
        channel.queue_declare(queue=rabbitmq_pressure_temperature_queue, durable=True)
        channel.basic_publish(exchange=rabbitmq_exchange, routing_key=rabbitmq_pressure_temperature_routing_key, body=message)

        # Close the RabbitMQ connection
        connection.close()
        print("Connection to RabbitMQ closed.")

    except Exception as e:
        print(f"Error connecting to RabbitMQ: {e}")

# Connect to Mosquitto
try:
    print("Connecting to Mosquitto...")
    client = mqtt.Client()
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    print("Connected to Mosquitto!")

    # Subscribe to the "pressure_temperature" topic
    print("Subscribing to 'pressure_temperature' topic...")
    client.subscribe("pressure_temperature", 1)
    print("Subscribed to 'pressure_temperature' topic!")

    # Start the MQTT loop
    print("Starting MQTT loop...")
    client.loop_forever()

except Exception as e:
    print(f"Error connecting to Mosquitto: {e}")
