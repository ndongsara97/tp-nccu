import json
import time
from google.cloud import pubsub_v1, firestore
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

# Google Cloud Pub/Sub settings
project_id = "tpnccu"  # replace with your GCP project ID
subscription_id = "nosqlsaver"
topic_name = "computation_result"

# Firestore settings
collection_name = "PTCRs"

# Create a subscriber client
subscriber = pubsub_v1.SubscriberClient()

# Create Firestore client
db = firestore.Client()

# Subscription path
subscription_path = subscriber.subscription_path(project_id, subscription_id)

def callback(message):
    logging.info(f"Received message ID: {message.message_id}")
    data = message.data.decode("utf-8")
    logging.debug(f"Message data: {data}")

    # Parse message data from JSON
    try:
        data_dict = json.loads(data)
        logging.debug("Parsed message data from JSON.")
    except json.JSONDecodeError as e:
        logging.error(f"Error parsing message data from JSON: {e}")
        data_dict = {"message": data}  # Fallback to raw data

    # Save data to Firestore
    try:
        doc_ref = db.collection(collection_name).document()
        doc_ref.set(data_dict)
        logging.info(f"Saved message ID {message.message_id} to Firestore.")
    except Exception as e:
        logging.error(f"Error saving message to Firestore: {e}")

    # Acknowledge the message
    message.ack()
    logging.info(f"Acknowledged message ID: {message.message_id}")

# Subscribe to the topic
try:
    subscriber.subscribe(subscription_path, callback=callback)
    logging.info(f"Listening for messages on {subscription_path}...")
except Exception as e:
    logging.error(f"Error subscribing to topic: {e}")

# Keep the main thread alive, or the process will exit
try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    logging.info("Stopping the subscriber.")

